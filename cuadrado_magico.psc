Algoritmo m_magica
	
	Dimension matriz[3, 3] //tama�o de mi matriz
	//Definir v_actual, n_fila, n_col Como Entero
	//Definir f_actual, c_actual, f_next, c_next Como Entero
	
	//defino mi numero de filas y columnas para la matriz 3x3
	filas<-3
	columnas<-3
	
	//variables inicializadas
	count<-0
	f_next<-0
	c_next<-0
	f_actual<-0
	c_actual<-0
	
	//la matriz tendra 3x3=9 elementos
	size = filas * columnas
	
	Dimension final[filas,columnas]
	
	// IPosicion en la mitad con un 1
	c_next<-(filas / 2) + 0.5
	f_next<-1
	f_actual<-nuevafila
	c_actual<-nuevaColumna
	final[f_next,c_next]<-1	
	count<-2
	
	//ciclo para llenar la matriz de acuerdo a las posiciones
	Para llenar = 1 Hasta size - 1 Con Paso 1 Hacer
		
		//movimiento hacia arriba
		Si ( f_next - 1 ) = 0 Entonces 
			f_next = filas
		SiNo
			f_next= f_next-1
		FinSi
		
		//movimiento hacia la derecha
		Si (c_next + 1) > columnas Entonces
			c_next = 1
		SiNo
			c_next = c_next + 1
		FinSi
		
		//Escribe el valor en la posicion
		Si final[f_next,c_next] == 0 Entonces			
			
			final[f_next,c_next] = count
			f_actual = f_next
			c_actual = c_next
			
		SiNo
			
			final[f_actual + 1, c_actual] = count //este almacena los valores 
			f_next= f_actual + 1
			c_next = c_actual
			
		FinSi
		
		count= count + 1
		
	Fin Para
	
	
	Para f = 1 Hasta filas Con Paso 1 Hacer
		
		Para c = 1 Hasta columnas Con Paso 1 Hacer
			
			Escribir "Valor para la posicion [",f,",",c,"]", " = ", final[f,c] //Matri final
			
		Fin Para
		
		
		
	Fin Para
	
	
	
	
FinAlgoritmo
